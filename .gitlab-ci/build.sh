mkdir -p /minecraft && cd /minecraft
apk update
apk --no-cache add wget git bash
wget -O /minecraft/BuildTools.jar "https://hub.spigotmc.org/jenkins/job/BuildTools/lastSuccessfulBuild/artifact/target/BuildTools.jar"
echo Version: $1
java -jar BuildTools.jar --rev $1
mv /minecraft/spigot-*.jar /builds/cmunroe/docker-bukkit/
if ls /minecraft/craftbukkit-* 1> /dev/null 2>&1; then
    echo Found craftbukkit.
else
    echo Missing craftbukkit.
    java -jar BuildTools.jar --compile craftbukkit --rev $1
fi
if ls /minecraft/craftbukkit-* 1> /dev/null 2>&1; then
    mv /minecraft/craftbukkit-*.jar /builds/cmunroe/docker-bukkit/
else
    mv /minecraft/CraftBukkit/target/craftbukkit-*.jar /builds/cmunroe/docker-bukkit/
fi

